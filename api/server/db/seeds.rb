# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

tascas = Tasca.create([
  {
    name: "Tasca do Aires", 
    address: "R. Manuel de Almeida e Sousa 312",
    rating: 8,
    image_url: "https://scontent.flis9-1.fna.fbcdn.net/v/t31.18172-8/175559_360614534031748_430978973_o.jpg?_nc_cat=104&ccb=1-3&_nc_sid=174925&_nc_ohc=FuqilqLdhRwAX-NAs_U&_nc_ht=scontent.flis9-1.fna&oh=940eb8d930c1b4ce8db945ef8e88ccb5&oe=60DD9BD4"
  }, 
  {
    name: "Casa Chelense", 
    address: "R. das Rãs 1",
    rating: 5,
    image_url: "https://scontent.flis9-1.fna.fbcdn.net/v/t1.6435-9/126881575_2247276612071495_3592945396203339839_n.jpg?_nc_cat=100&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=BaWQnq5VsqAAX8CYEjF&_nc_ht=scontent.flis9-1.fna&oh=3c5a10d01e5fddb339e66466d7f0cd5d&oe=60DE23EB"
  }, 
  {
    name: "Casa Costa", 
    address: "R. Augusto Filipe Simões 3",
    rating: 8,
    image_url: "https://scontent.flis9-1.fna.fbcdn.net/v/t1.18169-9/10155082_10152386638523914_3332437792048373623_n.jpg?_nc_cat=108&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=htHYOIdnJJgAX-fk4lu&_nc_ht=scontent.flis9-1.fna&oh=44eadb512553ed9deeffe7cc48b5115a&oe=60DFFF75"
  }, 
])