import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import FormGroup from 'react-bootstrap/esm/FormGroup';
import ImageUpload from './ImageUpload/ImageUpload';

const TascaCreate = ({
  tasca,
  setTasca,
  toggleCreateForm,
  validated,
  handleSubmit,
}) => {
  const [image, setImage] = useState('');

  //handle user input change
  const handleChange = (event) => {
    event.preventDefault();
    setTasca(
      Object.assign({}, tasca, {
        [event.target.id]: event.target.value,
        image_url: image,
      })
    );
  };

  return (
    <>
      <Container className='border rounded my-4'>
        <Form
          noValidate
          validated={validated}
          onSubmit={handleSubmit}
          className='p-3'
        >
          <Row>
            <Col sm={9}>
              <Form.Group>
                <Form.Label>Name</Form.Label>
                <Form.Control
                  required
                  type='text'
                  placeholder='Tasca name'
                  id='name'
                  name='name'
                  onChange={handleChange}
                />
                <Form.Control.Feedback type='invalid'>
                  Please provide a name
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group>
                <Form.Label>Address</Form.Label>
                <Form.Control
                  required
                  type='text'
                  placeholder='Tasca address'
                  id='address'
                  name='address'
                  onChange={handleChange}
                />
                <Form.Control.Feedback type='invalid'>
                  Please provide an address
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group>
                <Form.Label>Rating</Form.Label>
                <Form.Control
                  required
                  as='select'
                  id='rating'
                  name='rating'
                  defaultValue={''}
                  onChange={handleChange}
                >
                  <option disabled></option>
                  <option value='1'>1</option>
                  <option value='2'>2</option>
                  <option value='3'>3</option>
                  <option value='4'>4</option>
                  <option value='5'>5</option>
                  <option value='6'>6</option>
                  <option value='7'>7</option>
                  <option value='8'>8</option>
                  <option value='9'>9</option>
                  <option value='10'>10</option>
                </Form.Control>
                <Form.Control.Feedback type='invalid'>
                  Please provide a rating
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
            <Col sm={3}>
              <FormGroup>
                <Form.Label>Upload Image</Form.Label>
                <ImageUpload image={image} setImage={setImage} id='image_url' />
              </FormGroup>
            </Col>
          </Row>
          <Button variant='success' type='submit'>
            Add Tasca
          </Button>

          <Button
            className='ml-3'
            variant='outline-secondary'
            onClick={toggleCreateForm}
            onChange={handleChange}
          >
            Cancel
          </Button>
        </Form>
      </Container>
    </>
  );
};

export default TascaCreate;
