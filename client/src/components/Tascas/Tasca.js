import React from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const Tasca = ({ id, attributes: { name, address, rating }, deleteTasca }) => {
  return (
    <>
      <Row className='border-top py-2'>
        <Col xs={3} md={3}>
          <Link to={`/tascas/view/${id}`}>{name}</Link>
        </Col>
        <Col xs={3} md={5}>
          {address}
        </Col>
        <Col xs={2} md={2}>
          {rating}
        </Col>
        <Col xs={4} md={2} className='d-flex justify-content-between'>
          <Col>
            <Link to={`/tascas/edit/${id}`}>
              <Button variant='outline-primary'>Edit</Button>
            </Link>
          </Col>
          <Col>
            <Button variant='outline-danger' onClick={() => deleteTasca(id)}>
              Delete
            </Button>
          </Col>
        </Col>
      </Row>
    </>
  );
};

export default Tasca;
