import React, { useState, useEffect } from 'react';
import SearchBar from '../SearchBar/SearchBar';
import Tasca from './Tasca';
import TascaCreate from '../TascaCreate';

import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/esm/Dropdown';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import axios from 'axios';

const Tascas = () => {
  const [tasca, setTasca] = useState({});
  const [tascas, setTascas] = useState([]);
  const [search, setSearch] = useState('');
  const [sorted, setSorted] = useState(false);
  const [validated, setValidated] = useState(false);
  const [showCreateForm, setShowCreateForm] = useState(false);

  useEffect(() => {
    showTascas();
  }, []);

  //show all tascas - GET
  const showTascas = async () => {
    try {
      const res = await axios.get('/api/v1/tascas.json');
      setTascas(res.data.data);
    } catch (err) {
      console.log(err);
    }
  };

  //create new tasca - POST
  const createTasca = async () => {
    try {
      const res = await axios.post('api/v1/tascas', { tasca });
      setTascas((prevTascas) => [...prevTascas, res.data.data]);
    } catch (err) {
      console.log(err);
    }
  };

  //delete tasca - DELETE
  const deleteTasca = async (id) => {
    try {
      await axios.delete(`/api/v1/tascas/${id}`);
      setTascas((prevTascas) => prevTascas.filter((tasca) => tasca.id !== id));
    } catch (err) {
      console.log(err);
    }
  };

  //form submit handler
  const handleSubmit = (event) => {
    event.preventDefault();
    const form = event.currentTarget;

    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      createTasca();
      setShowCreateForm(!showCreateForm);
    }
    setValidated(true);
  };

  //show/hide create tasca form
  const toggleCreateForm = () => {
    setShowCreateForm(!showCreateForm);
    setValidated(false);
  };

  //refresh page - refresh button onClick
  const refreshPage = () => {
    window.location.replace(window.location.href);
  };

  //search input handler
  const handleSearchInput = (event) => {
    setSearch(event.target.value);
  };

  //sort tascas by rating high -> low
  const handleSortHigh = () => {
    tascas.sort((a, b) => (a.attributes.rating > b.attributes.rating ? -1 : 1));
    setSorted(!sorted);
  };

  //sort tascas by rating low -> high
  const handleSortLow = () => {
    tascas.sort((a, b) => (a.attributes.rating > b.attributes.rating ? 1 : -1));
    setSorted(!sorted);
  };

  return (
    <>
      <Container className='my-3'>
        <Row className='py-2 justify-content-center'>
          <h1>Tascas!</h1>
        </Row>
        <Row className='py-2'>
          <Col xs={8} md={10}>
            <SearchBar
              handleSearchInput={handleSearchInput}
              setSearch={setSearch}
            />
          </Col>
          <Col xs={4} md={2}>
            <DropdownButton variant='outline-primary' title='Sort by Rating'>
              <Dropdown.Item onClick={handleSortHigh}>
                Highest Rated
              </Dropdown.Item>
              <Dropdown.Item onClick={handleSortLow}>
                Lowest Rated
              </Dropdown.Item>
            </DropdownButton>
          </Col>
        </Row>
        <Row className='mt-4 pt-2'>
          <Col xs={3} md={3}>
            <h6>Name</h6>
          </Col>
          <Col xs={3} md={5}>
            <h6>Address</h6>
          </Col>
          <Col xs={2} md={2}>
            <h6>Rating</h6>
          </Col>
          <Col xs={4} md={2}>
            <h6>Actions</h6>
          </Col>
        </Row>
        {tascas
          .filter((tasca) => {
            if (search === '') {
              return tasca;
            } else if (
              tasca.attributes.name.toLowerCase().includes(search.toLowerCase())
            ) {
              return tasca;
            }
            return false;
          })

          .map((tasca) => {
            return (
              <Tasca
                key={tasca.attributes.name}
                tasca={tasca}
                id={tasca.id}
                attributes={tasca.attributes}
                deleteTasca={deleteTasca}
              />
            );
          })}
        <Row className='d-flex justify-content-between border-top py-2'>
          <Button className='ml-3' variant='primary' onClick={toggleCreateForm}>
            Create
          </Button>
          <Button variant='outline-secondary' onClick={refreshPage}>
            Refresh
          </Button>
        </Row>
      </Container>
      {showCreateForm ? (
        <TascaCreate
          tasca={tasca}
          setTasca={setTasca}
          toggleCreateForm={toggleCreateForm}
          handleSubmit={handleSubmit}
          validated={validated}
        />
      ) : null}
    </>
  );
};

export default Tascas;
