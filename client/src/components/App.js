import React from 'react';
import { Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Tascas from './Tascas/Tascas';
import TascaView from './TascaView';
import TascaEdit from './TascaEdit';

const App = () => {
  return (
    <Switch>
      <Route exact path='/' component={Tascas} />
      <Route exact path='/tascas/view/:id' component={TascaView} />
      <Route
        exact
        path='/tascas/edit/:id'
        render={(props) => <TascaEdit {...props} name='test' />}
      />
    </Switch>
  );
};

export default App;
